# word-lists

Lists of words in JSON files. Can be used as whimsical random names, notes, placeholders, and the like.
